-- postgres db schema
SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = ON;
SET check_function_bodies = FALSE;
SET client_min_messages = WARNING;

CREATE TABLE Ingredient (
    id UUID PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    alcoholic boolean not null,
    volume int not null
);

CREATE TABLE Glass (
    id BIGINT PRIMARY KEY NOT NULL,
    name TEXT NOT NULL
);

CREATE TABLE Cocktail (
    id UUID PRIMARY KEY NOT NULL ,
    ingredients UUID references ingredient(id),
    glass BIGINT NOT NULL REFERENCES glass(id) ,
    instructions TEXT NOT NULL,
    imgUrl TEXT NOT NULL,
    rating INTEGER NOT NULL
);