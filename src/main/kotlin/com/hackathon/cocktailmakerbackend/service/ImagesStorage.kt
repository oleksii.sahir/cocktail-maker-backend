package com.hackathon.cocktailmakerbackend.service

import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.AmazonS3Exception
import com.amazonaws.services.s3.model.ObjectMetadata
import com.hackathon.cocktailmakerbackend.config.AppConfig
import org.springframework.stereotype.Service
import java.io.File
import java.net.URL
import java.net.URLEncoder

@Service
class ImagesStorage(private val amazonS3Client: AmazonS3Client, private val appConfig: AppConfig) {
    fun upload(imageUrl: String): String {
        val fileName = imageUrl.split("/").last()
        val url = URL(imageUrl)
        val path = "${appConfig.imagesFolder}/$fileName"
        try {
            amazonS3Client.doesObjectExist(appConfig.bucketName, path)
        } catch (e: AmazonS3Exception) {
            if (e.statusCode == 404) {
                val data = ObjectMetadata()
                data.contentType = "image/jpeg"
                amazonS3Client.putObject(appConfig.bucketName, path, url.openStream(), data)
            } else throw e
        }
        return appConfig.imagesDomain + "/" + path
    }

    fun uploadFile(file: File): String {
        val path = "${appConfig.imagesFolder}/${file.name}"
        if (!amazonS3Client.doesObjectExist(appConfig.bucketName, path)) {
            val data = ObjectMetadata()
            data.contentType = "image/${file.extension}"
            amazonS3Client.putObject(appConfig.bucketName, path, file.inputStream(), data)
        }
        return appConfig.imagesDomain + "/" + URLEncoder.encode(path)
    }
}
