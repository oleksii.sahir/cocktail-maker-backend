package com.hackathon.cocktailmakerbackend.controller

import com.hackathon.cocktailmakerbackend.model.CocktailModel
import com.hackathon.cocktailmakerbackend.model.IngredientModel
import com.hackathon.cocktailmakerbackend.model.SearchRequest
import com.hackathon.cocktailmakerbackend.service.CocktailService
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/api/v1/cocktails")
@CrossOrigin(originPatterns = ["*"], allowedHeaders = ["*"], exposedHeaders = ["*"])
class CocktailController(private val cocktailService: CocktailService) {
    @GetMapping
    fun cocktails(): List<CocktailModel> {
        return cocktailService.cocktails()
    }

    @PostMapping
    fun saveCocktails(@RequestBody cocktailModel: CocktailModel) {
        cocktailService.saveCocktail(cocktailModel)
    }

    @GetMapping("fuzzy-search/{name}")
    fun fuzzySearch(@PathVariable("name") name: String): List<CocktailModel> {
        return cocktailService.fuzzySearch(name)
    }

    @PostMapping("/search")
    fun search(@RequestBody ingredients: List<UUID>): List<CocktailModel> {
        return cocktailService.searchCocktailsByIngredients(ingredients)
    }
}
