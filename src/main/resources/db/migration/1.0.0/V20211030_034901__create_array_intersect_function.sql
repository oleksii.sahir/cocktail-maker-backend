create function array_intersect(a1 text[], a2 text[]) returns text[] as $$
declare
ret text[];
begin
    if a1 is null then
        return a2;
    elseif a2 is null then
        return a1;
end if;
select array_agg(e) into ret
from (
         select unnest(a1)
         intersect
         select unnest(a2)
     ) as dt(e);
return ret;
end;
$$ language plpgsql;