package com.hackathon.cocktailmakerbackend.service

import com.hackathon.cocktailmakerbackend.model.CocktailModel
import com.hackathon.cocktailmakerbackend.repository.CocktailRepository
import org.springframework.stereotype.Service
import java.util.*
import java.util.stream.Collectors


@Service
class CocktailService(private val cocktailRepository: CocktailRepository) {

    fun cocktails(): List<CocktailModel> {
        return cocktailRepository.findAll().map { it.toModel() }
    }

    fun saveCocktail(cocktailModel: CocktailModel) {
        cocktailRepository.save(cocktailModel.toEntity())
    }

    fun fuzzySearch(name: String): List<CocktailModel> {
        return cocktailRepository.fuzzySearch(name).map { it.toModel() }
    }

    fun searchCocktailsByIngredients(ingredients: List<UUID>): List<CocktailModel> {
        val uuids = cocktailRepository.searchCocktailsByIngredients(
            ingredients.joinToString(
                separator = ",",
                transform = { it.toString() })
        )
            .map { UUID.fromString(it) }
        return cocktailRepository.findAllById(uuids)
                .map { it.toModel() }
                .map { it.enrichMissedIngredients(ingredients) }
                .stream()
                .sorted(CocktailModel::compareTo)
                .collect(Collectors.toList());
    }
}
