ALTER TABLE cocktail ADD COLUMN name TEXT;

CREATE TABLE cocktails_ingridients
(
    cocktail_id   uuid not null references cocktail(id),
    ingridient_id uuid not null references ingredient(id),
    UNIQUE (cocktail_id, ingridient_id)
);
