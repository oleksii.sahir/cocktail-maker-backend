package com.hackathon.cocktailmakerbackend.model

import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

data class IngredientModel(
    var id: UUID,
    var name: String,
    var alcoholic: Boolean,
    var volume: String,
    var image: String
)
