package com.hackathon.cocktailmakerbackend.service

import com.hackathon.cocktailmakerbackend.model.IngredientModel
import com.hackathon.cocktailmakerbackend.repository.IngredientRepository
import org.springframework.stereotype.Service

@Service
class IngredientService(private val ingredientRepository: IngredientRepository) {

    fun ingredients(): List<IngredientModel> {
        return ingredientRepository.findAll().map { it.toModel() }
    }

    fun fuzzySearch(name: String): List<IngredientModel> {
        return ingredientRepository.fuzzySearch(name).map { it.toModel() }
    }

}
