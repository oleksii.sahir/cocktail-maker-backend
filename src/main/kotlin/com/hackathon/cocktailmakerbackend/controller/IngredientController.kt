package com.hackathon.cocktailmakerbackend.controller

import com.hackathon.cocktailmakerbackend.model.IngredientModel
import com.hackathon.cocktailmakerbackend.service.IngredientService
import org.springframework.web.bind.annotation.*
import javax.websocket.server.PathParam

@RestController
@RequestMapping("/api/v1/ingredients")
@CrossOrigin(originPatterns = ["*"], allowedHeaders = ["*"], exposedHeaders = ["*"])
class IngredientController(private val ingredientService: IngredientService) {

    @GetMapping
    fun ingredients(): List<IngredientModel> {
        return ingredientService.ingredients()
    }

    @GetMapping("fuzzy-search/{name}")
    fun fuzzySearch(@PathVariable("name") name: String): List<IngredientModel> {
        return ingredientService.fuzzySearch(name)
    }


}
