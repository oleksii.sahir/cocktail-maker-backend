package com.hackathon.cocktailmakerbackend.model

import com.hackathon.cocktailmakerbackend.domain.Glass
import com.hackathon.cocktailmakerbackend.domain.Ingredient
import java.util.*
import kotlin.collections.ArrayList

class CocktailModel(
    val id: UUID,
    val externalId: String,
    val name: String,
    var ingredients: List<IngredientModel> = ArrayList(),
    var glass: Glass,
    var instructions: String,
    var imgUrl: String?,
    var rating: Int,
    var missingIngredients: List<IngredientModel> = ArrayList()
) : Comparable<CocktailModel> {
    override operator fun compareTo(other: CocktailModel): Int {
        if (!this.missingIngredients.isEmpty() && other.missingIngredients.isEmpty()) return 1
        if (this.missingIngredients.isEmpty() && !other.missingIngredients.isEmpty()) return -1
        return 0
    }
}