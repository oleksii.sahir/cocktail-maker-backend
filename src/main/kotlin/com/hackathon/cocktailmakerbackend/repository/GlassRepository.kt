package com.hackathon.cocktailmakerbackend.repository

import com.hackathon.cocktailmakerbackend.domain.Glass
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface GlassRepository : JpaRepository<Glass, UUID> {
    fun findByName(name: String): Glass?
}
