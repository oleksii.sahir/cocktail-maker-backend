package com.hackathon.cocktailmakerbackend.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.web.client.RestTemplate

@Configuration
class ServicesConfig {
    @Bean
    fun restTemplate() = RestTemplate()
}