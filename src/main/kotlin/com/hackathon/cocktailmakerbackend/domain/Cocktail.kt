package com.hackathon.cocktailmakerbackend.domain

import java.util.*
import javax.persistence.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet

@Entity
@Table(name = "cocktail", schema = "public")
data class Cocktail(
    @Id
    val id: UUID,
    val externalId: String,
    var name: String,
    @ManyToMany
    @JoinTable(
        name = "cocktails_ingridients",
        joinColumns = [JoinColumn(name = "cocktail_id")],
        inverseJoinColumns = [JoinColumn(name = "ingridient_id")]
    )
    var ingredients: Set<Ingredient> = HashSet(),
    @ManyToOne
    @JoinColumn(name = "glass_id")
    var glass: Glass,
    var instructions: String = "",
    var imgUrl: String?,
    var rating: Int = 0
)