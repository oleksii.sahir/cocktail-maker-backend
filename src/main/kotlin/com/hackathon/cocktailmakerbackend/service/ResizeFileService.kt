package com.hackathon.cocktailmakerbackend.service

import org.springframework.stereotype.Service
import java.awt.Image
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO


@Service
class ResizeFileService {

    fun resize(file: File): File {

        val originalImage = ImageIO.read(file)
        val resultingImage: Image = originalImage.getScaledInstance(WIDTH, HEIGHT, Image.SCALE_DEFAULT)
        val outputImage = BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB)
        outputImage.graphics.drawImage(resultingImage, 0, 0, null)
        val outputFile = File(file.name);
        ImageIO.write(outputImage, file.extension, outputFile)

        return outputFile
    }

    companion object {
        const val WIDTH: Int = 547
        const val HEIGHT: Int = 547
    }
}
