package com.hackathon.cocktailmakerbackend

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication

@SpringBootApplication
@EnableConfigurationProperties
class CocktailMakerBackendApplication

fun main(args: Array<String>) {
	runApplication<CocktailMakerBackendApplication>(*args)
}
