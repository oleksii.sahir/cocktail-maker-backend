package com.hackathon.cocktailmakerbackend.service

import com.hackathon.cocktailmakerbackend.domain.Cocktail
import com.hackathon.cocktailmakerbackend.domain.Ingredient
import com.hackathon.cocktailmakerbackend.model.CocktailModel
import com.hackathon.cocktailmakerbackend.model.IngredientModel
import java.util.*
import java.util.stream.Collectors

fun Ingredient.toModel(): IngredientModel {
    return IngredientModel(this.id, this.name, this.alcoholic, this.volume, this.image)
}

fun IngredientModel.toEntity(): Ingredient {
    return Ingredient(this.id, this.name, this.alcoholic, this.volume, this.image)
}

fun Cocktail.toModel(): CocktailModel {
    return CocktailModel(
        id = this.id,
        externalId = this.externalId,
        name = this.name,
        ingredients = this.ingredients.map { it.toModel() },
        glass = this.glass,
        instructions = this.instructions,
        imgUrl = this.imgUrl,
        rating = this.rating
    )
}

fun CocktailModel.enrichMissedIngredients(searchedIngredients: List<UUID>): CocktailModel {
    return CocktailModel(
            id = this.id,
            externalId = this.externalId,
            name = this.name,
            ingredients = this.ingredients,
            glass = this.glass,
            instructions = this.instructions,
            imgUrl = this.imgUrl,
            rating = this.rating,
            missingIngredients = this.ingredients.stream()
                    .filter{!searchedIngredients.contains(it.id)}
                    .collect(Collectors.toList())
    )
}

fun CocktailModel.toEntity(): Cocktail {
    return Cocktail(
        id = this.id,
        externalId = this.externalId,
        name = this.name,
        ingredients = this.ingredients.map { it.toEntity() }.toSet(),
        glass = this.glass,
        instructions = this.instructions,
        imgUrl = this.imgUrl,
        rating = this.rating
    )
}
