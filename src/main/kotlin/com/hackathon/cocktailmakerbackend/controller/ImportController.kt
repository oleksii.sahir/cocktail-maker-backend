package com.hackathon.cocktailmakerbackend.controller

import com.hackathon.cocktailmakerbackend.service.ImportService
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/import")
@CrossOrigin(originPatterns = ["*"], allowedHeaders = ["*"], exposedHeaders = ["*"])
class ImportController(val importService: ImportService) {
    @GetMapping("/external")
    fun importExternal() = importService.importExternal()

    @GetMapping("/internal")
    fun importInternal() = importService.importInternal()

    @GetMapping("/internal/images")
    fun importInternalImages() = importService.importInternalImages()
}
