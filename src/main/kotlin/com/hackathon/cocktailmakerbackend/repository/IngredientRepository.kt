package com.hackathon.cocktailmakerbackend.repository

import com.hackathon.cocktailmakerbackend.domain.Ingredient
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface IngredientRepository : JpaRepository<Ingredient, UUID> {

    @Query(
        value = "select * from ingredient where ((soundex(name) = soundex(:name)) or (difference(name, :name) > 2))",
        nativeQuery = true
    )
    fun fuzzySearch(@Param("name") name: String): List<Ingredient>

    fun findByName(name: String): Ingredient?
}
