package com.hackathon.cocktailmakerbackend.model

import java.util.*

data class SearchRequest (
    var ingredients: List<UUID> = ArrayList()
)
