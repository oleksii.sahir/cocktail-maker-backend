package com.hackathon.cocktailmakerbackend.service

import com.fasterxml.jackson.databind.ObjectMapper
import com.hackathon.cocktailmakerbackend.domain.Cocktail
import com.hackathon.cocktailmakerbackend.domain.Glass
import com.hackathon.cocktailmakerbackend.domain.Ingredient
import com.hackathon.cocktailmakerbackend.dto.thecocktaildb.Drink
import com.hackathon.cocktailmakerbackend.dto.thecocktaildb.Drinks
import com.hackathon.cocktailmakerbackend.repository.CocktailRepository
import com.hackathon.cocktailmakerbackend.repository.GlassRepository
import com.hackathon.cocktailmakerbackend.repository.IngredientRepository
import org.slf4j.LoggerFactory
import org.springframework.core.io.Resource
import org.springframework.core.io.ResourceLoader
import org.springframework.stereotype.Service
import org.springframework.util.FileCopyUtils
import org.springframework.web.client.RestTemplate
import java.io.File
import java.io.IOException
import java.io.InputStreamReader
import java.io.UncheckedIOException
import java.net.URLEncoder
import java.nio.charset.Charset
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.reflect.full.memberProperties


@Service
class ImportService(
    private val restTemplate: RestTemplate,
    private val ingredientRepository: IngredientRepository,
    private val glassRepository: GlassRepository,
    private val cocktailRepository: CocktailRepository,
    private val s3Service: ImagesStorage,
    private val objectMapper: ObjectMapper,
    private val resourceLoader: ResourceLoader
) {
    private val log = LoggerFactory.getLogger(ImportService::class.java)

    companion object {
        const val INGRIDIENT_PREFIX = "strIngredient"
    }

    fun importExternal() {
        for (i in 'a'..'z') {
            val response = restTemplate.getForEntity(
                "https://www.thecocktaildb.com/api/json/v1/1/search.php?f=$i",
                String::class.java
            )
            log.info("Fetched drinks for '$i'")
            val body = response.body ?: "{}"
//            saveToFile(i.toString(), body)
            val result = objectMapper.readValue(body, Drinks::class.java)

            result.drinks?.map {
                process(it)
            }
        }
    }

    fun importInternalImages() {
        val files = resourceLoader.getResource("classpath:dataset/ingridients").file.listFiles()
            .filter { !it.isDirectory }
            .associateBy({ it.name.replaceFirst(".${it.extension}", "") }, { it })

        ingredientRepository.findAll()
            .forEach {
                if (files.containsKey(it.name)) {
                    files.get(it.name)
                        ?.let { f -> s3Service.uploadFile(f) }
                        .let { u -> it.image = u!! }
                    ingredientRepository.save(it)
                }
            }
    }

    fun importInternal() {
        for (i in 'a'..'z') {
            val resource = resourceLoader.getResource("classpath:dataset/coctails-${i}.json")
            log.info("Loaded drinks for '$i'")
            val body = asString(resource) ?: "{}"
            saveToFile(i.toString(), body)
            val result = objectMapper.readValue(body, Drinks::class.java)

            result.drinks?.map {
                process(it)
            }
        }
    }

    fun asString(resource: Resource): String? {
        try {
            InputStreamReader(
                resource.inputStream, Charset.defaultCharset()
            ).use { reader -> return FileCopyUtils.copyToString(reader) }
        } catch (e: IOException) {
            throw UncheckedIOException(e)
        }
    }

    private fun saveToFile(name: String, content: String) {
        val path: Path = Paths.get("/tmp/coctails-${name}.json")
        val strToBytes: ByteArray = content.toByteArray()

        Files.write(path, strToBytes)
    }

    private fun process(drink: Drink): Cocktail {
        val ingridientsList = mutableSetOf<Ingredient>()
        val cls = Drink::class
        cls.memberProperties.stream().forEach {
            val value = it.getValue(drink, it)
            if (it.name.startsWith(INGRIDIENT_PREFIX) && value != null) {
                val methodName = "getStrMeasure${it.name.removePrefix(INGRIDIENT_PREFIX)}"
                val measure: String = try {
                    String.format(
                        "%s", cls.java
                            .getMethod(methodName)
                            .invoke(drink)
                    )
                } catch (e: Exception) {
                    log.error("$methodName exception {}", e)
                    ""
                }
                log.info("Measure: {}", measure)
                val ingredient = findOrCreateIngridient(value.toString(), measure)
                ingridientsList.add(ingredient)
            }
        }
        return findOrCreateCocktail(
            drink.strDrink,
            drink.idDrink,
            ingridientsList,
            findOrCreateGlass(drink.strGlass),
            drink.strDrinkThumb
        )
    }

    private fun findOrCreateIngridient(name: String, measure: String): Ingredient {
        var ingredient = ingredientRepository.findByName(name)
        if (ingredient != null) {
            return ingredient
        }
        ingredient = Ingredient(UUID.randomUUID(), name = name, alcoholic = true, volume = measure, image = "")
        ingredientRepository.save(ingredient)
        return ingredient
    }

    private fun findOrCreateGlass(name: String?): Glass {
        val nameOfDefault = name ?: "glass"
        val glass = glassRepository.findByName(nameOfDefault) ?: Glass(UUID.randomUUID(), name = nameOfDefault)
        return glassRepository.save(glass)
    }

    private fun findOrCreateCocktail(
        name: String,
        extId: String,
        ingredients: Set<Ingredient>,
        glass: Glass,
        imageUrl: String?
    ): Cocktail {
        val img = if (imageUrl != null) storeImage(imageUrl) else null
        val cocktail = cocktailRepository.findByName(name)?.apply {
            this.imgUrl = img
            this.name = name
            this.ingredients = ingredients
            this.glass = findOrCreateGlass("test")
        } ?: Cocktail(
            id = UUID.randomUUID(),
            externalId = extId,
            name = name,
            ingredients = ingredients,
            glass = findOrCreateGlass(glass.name),
            imgUrl = img
        )

        return cocktailRepository.save(cocktail)
    }

    private fun storeImage(imageUrl: String): String {
        var exception = RuntimeException()
        for (i in 1..3) {
            try {
                return s3Service.upload(imageUrl)
            } catch (e: RuntimeException) {
                TimeUnit.SECONDS.sleep(i * 2L)
                exception = e
                continue
            }
        }
        throw exception
    }
}
