package com.hackathon.cocktailmakerbackend.domain

import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "ingredient", schema = "public")
data class Ingredient (
        @Id
        var id : UUID,
        var name : String,
        var alcoholic : Boolean,
        var volume : String,
        var image: String
)
