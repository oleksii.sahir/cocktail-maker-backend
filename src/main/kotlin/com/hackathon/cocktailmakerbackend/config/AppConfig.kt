package com.hackathon.cocktailmakerbackend.config

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Configuration

@Configuration
@ConfigurationProperties(prefix = "app")
class AppConfig {
    var bucketName: String = ""
    var imagesFolder: String = ""
    var imagesDomain: String = ""
}