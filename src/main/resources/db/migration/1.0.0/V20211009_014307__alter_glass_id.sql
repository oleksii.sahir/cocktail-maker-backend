ALTER TABLE cocktail DROP COLUMN glass;
ALTER TABLE glass DROP COLUMN id;
ALTER TABLE glass ADD COLUMN id uuid PRIMARY KEY NOT NULL;
ALTER TABLE cocktail ADD COLUMN glass_id uuid references glass(id);
