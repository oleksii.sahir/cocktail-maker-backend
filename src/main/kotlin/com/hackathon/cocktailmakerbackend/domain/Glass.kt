package com.hackathon.cocktailmakerbackend.domain

import java.util.*
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "glass", schema = "public")
data class Glass (
     @Id
     var id: UUID,
     var name : String
)