package com.hackathon.cocktailmakerbackend.repository

import com.hackathon.cocktailmakerbackend.domain.Cocktail
import com.hackathon.cocktailmakerbackend.domain.Ingredient
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface CocktailRepository : JpaRepository<Cocktail, UUID> {

    @Query(
        value = "select * from cocktail where ((soundex(name) = soundex(:name)) or (difference(name, :name) > 2))",
        nativeQuery = true
    )
    fun fuzzySearch(@Param("name") name: String): List<Cocktail>

    fun findByName(name: String): Cocktail?

    @Query(
            value = "SELECT cast(cocktail_id as text) as a " +
                    "FROM cocktails_ingridients " +
                    "GROUP BY cocktail_id " +
                    "HAVING array_length(array_agg(ingridient_id\\:\\:text),1) - array_length(array_intersect(array_agg(ingridient_id\\:\\:text),string_to_array(:ingredients, ',')),1) <= 1",
        nativeQuery = true
    )
    fun searchCocktailsByIngredients(@Param("ingredients") ingredients: String): List<String>

}
